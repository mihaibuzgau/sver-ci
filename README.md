# sver-ci

Semantic versioning gitlab CI templates.

## Example

```yaml
include:
  - project: 'aserto/sver-ci'
    ref: main
    file:
      - '/lib/gitlab/templates/Sver-Tags.gitlab-ci.yml'
      - '/lib/gitlab/templates/Sver-Image-Tags.gitlab-ci.yml'

stages:
  - build

variables:
  SVER_DOCKER_REGISTRY: aserto-dev/sver
  SVER_DOCKER_SERVER: ghcr.io

print-version:
  stage: build
  needs: 
    - sver-image-tags
  dependencies:
    - sver-image-tags
  script:
    - echo "version is '$SVER_IMAGE_VERSION'"
    - echo "version is '$SVER_VERSION_VERSION'"
```

## Inputs

### SVER_PRE_RELEASE

**Optional**. If specified, this pre release reference will be added to the calculated semantic version.

### SVER_DOCKER_REGISTRY

**Optional**. If specified, sver will calculate the tags for this docker registry.

### SVER_DOCKER_SERVER

**Optional**. Server for the docker registry.

### SVER_DOCKER_USERNAME

**Optional**. Username for connecting to the docker server. 

### SVER_DOCKER_PASSWORD

**Optional**. Password for connecting to the docker server.

### SVER_NEXT

**Optional**. Indicates whether to output the next version of the repository. Possible values are 'major', 'minor' or 'patch'. Ignored if `docker_image` is set.

### SVER_IMAGE_VERSION

**Optional**. The version of the sver docker image to use.

When using this to calculate tags, the output is a multiline string stored in:
* SVER_IMAGE_VERSION for the `sver-image-tags` template
* SVER_VERSION for the `sver-tags` template
each tag is on a separate line.